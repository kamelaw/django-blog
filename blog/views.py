from django.shortcuts import render


# Create your views here.

posts = [
    {
        'author': 'Kam',
        'title': 'Blog post 1',
        'content': 'Content',
        'date_posted': 'March 14, 2021'
    },
    {
        'author': 'Lexi',
        'title': 'Blog post 2',
        'content': 'Content',
        'date_posted': 'March 14, 2021'
    }
]


def index(request):
    context = {
        'posts': posts
    }
    return render(request, 'index.html', context)


def about(request):
    return render(request, 'about.html', {})